# recipe-app-api-proxy

NGNIX proxy app for our recipe app API

## Usage 

### Environment variables

    * 'LISTEN_PORT' - Port to listen on (Default: '8000')
    * 'APP_HOST' - Hostname of the app to forward requests to (Default: 'app')
    * 'APP_PORT' - Port of the app to forward requests to (Default: '9000')